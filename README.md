# LIBS

This is a submodule of **Tadah!** and is not intended to be used on its own (although it is technically possible).

If you're interested in running **LAMMPS simulations** with Tadah! potentials, visit [Tadah!LAMMPS](https://git.ecdf.ed.ac.uk/tadah/tadah.lammps). For those interested in **developing machine learning interatomic potentials**, see [Tadah!MLIP](https://git.ecdf.ed.ac.uk/tadah/tadah.mlip).

Please refer to the online documentation for more information:

[https://tadah.readthedocs.io/](https://tadah.readthedocs.io/)